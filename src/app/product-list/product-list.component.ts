import { Component, OnInit } from '@angular/core';
import { Product } from '../products';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  products: Array<Product> = [];

  constructor() {
    this.products.push({
      name: 'Vinil Duplo Ariana Grande – Thank u, next ',
      desc: 'O quinto álbum de estúdio em vinil da cantora Ariana Grande: thank u, next, sucessor do Sweetener. Com com os sucessos thank u, next e 7 rings',
      price: 459.6,
    });
    this.products.push({
      name: 'Vinil Taylor Swift – Folklore 1. The “In The Trees”',
      desc: 'O oitavo álbum de Taylor Swift, folklore in the trees. O álbum que foi lançado em julho de 2020, possui 17 faixas, incluindo o single  “Cardigan”.',
      price: 567.9,
    });
    this.products.push({
      name: 'CD Olivia Rodrigo – SOUR',
      desc: 'Sour é o álbum em CD de estreia da cantora estadunidense Olivia Rodrigo, lançado em 21 de maio de 2021 através da Geffen Records. O álbum foi desenvolvido durante a quarentena da pandemia de COVID-19, com a produção sendo realizada por Dan Nigro',
      price: 34.9,
    });
  }

  ngOnInit() {}

  getProducts() {
    if (this.products.length != 0) {
      return this.products;
    }
  }

  share() {
    window.alert('O produto foi compartilhado!');
  }
  onNotify() {
    window.alert('Você será notificado quando o produto estiver em promoção');
  }
}
